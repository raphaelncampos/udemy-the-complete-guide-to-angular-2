# Section 1: Getting Started

#### 1 - 6 - Project setup.

I'm using nvm on ubuntu to manage my Node.js versions.
- Install nvm, node and angular-cli
```shell script
$ curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
$ echo -e 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"\n[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm\n[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion' >> ~/zshrc
$ nvm install --lts
$ npm install -g @angular/cli
```

- generate our new angular project 
```shell script
$ cd section-1-getting-started/code
$ ng new my-first-app # press enter twice
```

#### 7. Editing the first app

To use ngModel in view, we must import the **FormsModule** in our *app.component.ts*
```typescript
...
import {FormsModule} from "@angular/forms";

@NgModule({
  ...
  imports: [
    ...
    FormsModule
  ],
 ...

```

#### 11. A basic project setup using bootstrap for styling

- Installing bootstrap on our project
```shell script
$ cd section-1-getting-started/code
$ npm install --save bootstrap@3
```

- To use bootstrap styles in angular, go to **angular.json** file and add...:

```
"architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            ...
            "assets": [
              ...
            ],
            "styles": [
              "src/styles.css",
              "node_modules/bootstrap/dist/css/bootstrap.min.css"
            ],
            ...
```
