# Glossary

### AngularJS

Prior version of Angular.

### Angular

Rewritten Javascript Framework to create Single Page Applications.

### SPA
### Single Page Applications

Just one HTML file that gets changed by the javascript sources.

### CLI

Command line interface.

### Angular CLI

A tool to initialize, develop, scaffold and maintain Angular applications.

### Node.js

Javascript runtime

### npm

Node Package Manager. A tool to install node modules, like Angular CLI.

### nvm

Node Version Manager. Used on Ubuntu to manage several versions of `npm`. 

### ng

The binary of the angular cli.

### Tyescript

A typed superset of javascript that compiles to plain javascript.

### data binding

The binding between the typescript source and the html view.

### [(ngModel)]

Directive used to bind a typescript property to a HTML element. 

### Angular module

Sub packages from angular, must be imported from the typescript packages of "@angular/"

### Bootstrap

A HTML/CSS/JS library (in our case, provided as a node module).


